#preflight:
    #Create unmanaged solution - note name of solution (SolutionUniqueName)
    #define $global:solutionUniqueNameForExport variable
    #Define new entity params in .\EntityDefinitions.xlsx
    #Define new attribute params in .\AttributeTemplate.xlsx

#run
    #enter admin Dataverse credentials when prompted
        #select display list of available orgs; choose dataverse for teams CDS environment for appropriate team
        # https://www.procrm.ch/?p=817
        
#postflight:
    # Using XrmTools  ( https://www.xrmtoolbox.com/documentation/ ), install plugin Attributes Factory  from Tools Library ( https://github.com/MscrmTools/Javista.AttributesFactory )
    # Define CDS connection string using Connect > New Connection (Connect using connection wizard)
    # Use string for server associated with Dataverse for Teams CDS instance (see output at end of script run)

    # Open attributes factory tool
        #select environment to create attributes > solutionUniqueNameForExport
        #select Attribute Template Excel file
        #Proces Attributes
        #return to this script and press enter to re-import/export solution and apply changes