#preflight:
    #Create unmanaged solution - note name of solution (SolutionUniqueName)
    #define $global:solutionUniqueNameForExport variable
    #Define new entity params in .\EntityDefinitions.xlsx
    #Define new attribute params in .\AttributeTemplate.xlsx

#run
    #enter admin Dataverse credentials when prompted
        #select display list of available orgs; choose dataverse for teams CDS environment for appropriate team
        # https://www.procrm.ch/?p=817
#postflight:
    # Using XrmTools  ( https://www.xrmtoolbox.com/documentation/ ), install plugin Attributes Factory  from Tools Library ( https://github.com/MscrmTools/Javista.AttributesFactory )
    # Define CDS connection string using Connect > New Connection (Connect using connection wizard)
    # Use string for server associated with Dataverse for Teams CDS instance (see output at end of script run)

    # Open attributes factory tool
        #select environment to create attributes > solutionUniqueNameForExport
        #select Attribute Template Excel file
        #Proces Attributes
        #return to this script and press enter to re-import/export solution and apply changes

#Install Module
if ( $null -eq ( get-module -name microsoft.xrm.data.powershell | select-object Version ) )
{
    install-module microsoft.xrm.data.powershell -scope CurrentUser -allowclobber
}
if ( $null -eq ( get-module -name ImportExcel | select-object Version ) )
{
    install-module ImportExcel -scope CurrentUser -allowclobber
}
#Load Dataverse PowerShell Modules
Import-Module Microsoft.Xrm.Data.Powershell

#connect to Dataverse interactively:
Connect-CrmOnlineDiscovery -InteractiveMode

#set unmanaged solution name here
$global:solutionUniqueNameForExport = Read-Host -Prompt "Enter solution name to import/export to"

#get record for Dataverse for Teams Solution
# $global:DataverseForTeamsDefaultSoln = (Get-CrmRecords -EntityLogicalName solution -Fields *).CrmRecords | Where-Object -Property friendlyname -eq "Common Data Services Default Solution"
function CreateNewEntity() {

    param (   
        [string]$entityname,
        [string]$entityPluralName = $entityname + 's',
        [string]$entityschemaname = $entityname.trim(),
        [string]$entityprefix = 'ptm',
        [string]$entityDescription = 'table created by PTM',
        [string]$SolutionUniqueName = $null,
        [string]$primaryAttributeFieldName = 'Name',
        [string]$primaryAttributeMaxChars = '100'
    )
    if ($null -eq $SolutionUniqueName -or '' -eq $SolutionUniqueName )
    {
        Write-Output "Creating new entity $entityname in Default Solution..."
    }
    else
    {
        Write-Output "Creating new entity $entityname in Default Solution and Unmanaged Solution: $SolutionUniqueName..."
    }

    $newentity = New-Object Microsoft.Xrm.Sdk.Metadata.EntityMetadata
    $newentity.SchemaName = $entityprefix+"_"+$entityschemaname
    $newentity.DisplayName = New-Object Microsoft.Xrm.Sdk.Label($entityname, 1033)
    $newentity.DisplayCollectionName = New-Object Microsoft.Xrm.Sdk.Label($entityPluralName,1033)
    $newentity.Description = New-Object Microsoft.Xrm.Sdk.Label($entityDescription,1033)
    $newentity.OwnershipType = [Microsoft.Xrm.Sdk.Metadata.OwnershipTypes]::UserOwned
    $newentity.IsActivity = $false

    $primattribute = New-Object Microsoft.Xrm.Sdk.Metadata.StringAttributeMetadata
    $primattribute.SchemaName = $entityprefix+"_"+$primaryAttributeFieldName
    $primattribute.DisplayName = New-Object Microsoft.Xrm.Sdk.Label($primaryAttributeFieldName,1033)
    $primattribute.Format = [Microsoft.Xrm.Sdk.Metadata.StringFormat]::Text
    $primattribute.MaxLength = $primaryAttributeMaxChars

    $request = New-Object Microsoft.Xrm.Sdk.Messages.CreateEntityRequest
    $request.Entity = $newentity
    $request.PrimaryAttribute = $primattribute
    if (!($null -eq $SolutionUniqueName -or '' -eq $SolutionUniqueName ))
    {
        $request.SolutionUniqueName = $SolutionUniqueName
    }

    $response = $conn.Execute($request)

    Write-Output ""
    Write-Output "... entity $entityname created."

    return $response
}

Class DataverseEntityDef
{
    [string]$entityName
    [string]$entitySchemaName
    [string]$entityPrefix
    [string]$entityDescription
    [string]$SolutionUniqueName = $null
    [string]$primaryAttributeFieldName = 'Name'
    [string]$entityPluralName
    [string]$primaryAttributeMaxChars

    [string] Create()
    {
        if ($null -eq $this.SolutionUniqueName)
        {
            CreateNewEntity -entityname $this.entityName -entityschemaname $this.entitySchemaName -entitypluralname $this.entityPluralName -entityDescription $this.entityDescription -entityprefix $this.entityPrefix -primaryAttributeFieldName $this.primaryAttributeFieldName -primaryAttributeMaxChars $this.primaryAttributeMaxChars
            return "Entity Created: " + $this.entityprefix +"_" + $this.entitySchemaName + " ($($this.entityName))"
        }
        else
        {
            CreateNewEntity -entityname $this.entityName -entityschemaname $this.entitySchemaName -entitypluralname $this.entityPluralName -entityDescription $this.entityDescription -entityprefix $this.entityPrefix -primaryAttributeFieldName $this.primaryAttributeFieldName -primaryAttributeMaxChars $this.primaryAttributeMaxChars -SolutionUniqueName $this.SolutionUniqueName
            return "Entity Created: " + $this.entityprefix +"_" + $this.entitySchemaName + " ($($this.entityName))" + " Solution Name (Unmanaged): " + $this.SolutionUniqueName 
        }
        
    }
}

#Load Table Definitions from Excel Template in .\CreateTablesAndAddFieldsToDataverse
$entityDefinitions = Import-Excel -Path .\EntityDefinitions.xlsx

#add tables to solution
foreach ($entityDefinition in $entityDefinitions)
{
    $entdef                           = New-Object DataverseEntityDef
    $entdef.entityName                = $entityDefinition.entityName
    $entdef.entityPrefix              = $entityDefinition.entityprefix
    $entdef.entityDescription         = $entityDefinition.entityDescription
    $entdef.entitySchemaName          = $entityDefinition.entitySchemaName
    $entdef.SolutionUniqueName        = $entityDefinition.SolutionUniqueName
    $entdef.primaryAttributeFieldName = $entityDefinition.primaryAttributeFieldName
    $entdef.primaryAttributeMaxChars  = $entityDefinition.primaryAttributeMaxChars
    $entdef.entityPluralName          = $entityDefinition.entityPluralName
    $entdef.Create()
}
#Export Solution
$datestamp = get-date -Format "yyyy-MM-dd_HHmm"
Export-CrmSolution -SolutionName $global:solutionUniqueNameForExport -SolutionFilePath ".\" -SolutionZipFileName "$($global:solutionUniqueNameForExport)_$datestamp.zip"

#Import and publish changes for entries to appear in Dataverse for Teams
Import-CrmSolution -SolutionFilePath ".\$($global:solutionUniqueNameForExport)_$(get-date -Format "yyyy-MM-dd_HHmm").zip" -PublishChanges

#show the connection info for XrmToolbox:
# Write-Host "Look for connection info for Dataverse for Teams Environment."
# Write-Host "server = WebApplicationUrl corresponding to friendly name = Dataverse for Teams team name"
# Get-CrmOrganizations -DeploymentRegion NorthAmerica -OnLineType Office365
# pause
Write-Host "Complete post-flight steps in comment at top of script, then press return"
pause
Export-CrmSolution -SolutionName $global:solutionUniqueNameForExport -SolutionFilePath ".\" -SolutionZipFileName "$($global:solutionUniqueNameForExport)_$datestamp.zip"

#Import and publish changes for entries to appear in Dataverse for Teams
Import-CrmSolution -SolutionFilePath ".\$($global:solutionUniqueNameForExport)_$datestamp.zip" -PublishChanges
pause